--declare @ts datetime
--set @ts = GETDATE()
--select @ts
--insert into sf.SalesRep (RowStatus,RowCreationTime,Id,FirstName,LastName,UserId,[Status]) values (1,@ts,'WGS\WN00135025','LORENZI','MATTEO','WGS\WN00073509',0)
--update sf.SalesRep set [Status]=1 where Id='WGS\WN00135025' 

----------------------------------------------------------------------------------------------------------------------------
--could you please also add salesrep Lorenzi Matteo --WGS\WN00135025
--to area manager Federico Libera . Thanks a lot
--select * from sf.[User] where Id = 'WGS\WN00073509' --Federico Libera
--select * from  sf.SalesRep where LastName like '%LORENZI%' and FirstName like '%MATTEO%' and Id = 'WGS\WN00135025'
----------------------------------------------------------------------------------------------------------------------------

--insert into sf.SalesRep (RowStatus,RowCreationTime,Id,FirstName,LastName,UserId,[Status]) values (1,@ts,'WGS\WN00128932','AMADORI','FRANCESCO','WGS\WN00071527',0)
--insert into sf.SalesRep (RowStatus,RowCreationTime,Id,FirstName,LastName,UserId,[Status]) values (1,@ts,'WGS\WN00073027','SPADONI','DANIEL PABLO','WGS\WN00071527',0)

----update statements
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%LONGARINI%' and FirstName like '%NICOLA%'				 and Id = 'WGS\WN00072165'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%BECCACECE%' and FirstName like '%MARCO%'				 and Id = 'WGS\WN00084127'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%ANGELOZZI%' and FirstName like '%FRANCESCO%'			 and Id = 'WGS\WN00098694'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%FORNAROLA%' and FirstName like '%LUCA%'				 and Id = 'WGS\WN00103455'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%IALEGGIO%' and FirstName like '%FABIO%'				 and Id = 'WGS\WN00128886'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%BOTTICELLI%' and FirstName like '%MARCO%'				 and Id = 'WGS\WN00128898'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%RICHIEDEI%' and FirstName like '%ROBERTO%'			 and Id = 'WGS\WN00072816'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%GIOVANNANGELI%' and FirstName like '%WALTER%'			 and Id = 'WGS\WN00072098'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%PRIORI%' and FirstName like '%ALBERTO%'				 and Id = 'WGS\WN00072731'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%NAPOLETANI%' and FirstName like '%PAOLO%'				 and Id = 'WGS\WN00072479'
--update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%GABRIELLI%' and FirstName like '%TIZIANO%'			 and Id = 'WGS\WN00071981'

----update statements
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%TANCREDI%' and FirstName like '%PIERO%'and Id = 'WGS\WN00073104'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%D%ALESIO%' and FirstName like '%MATTEO%'and Id = 'WGS\WN00071788'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%GRASSINI%' and FirstName like '%ANDREA%'and Id = 'WGS\WN00112226'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%MARABESE%' and FirstName like '%ANTONIO%'and Id = 'WGS\WN00112284'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%COLELLA%' and FirstName like '%PAUL%'and Id = 'WGS\WN00125386'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%FACCANI%' and FirstName like '%FRANCESCO%'and Id = 'WGS\WN00071866'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%SOLOFRA%' and FirstName like '%FRANCESCO%'and Id = 'WGS\WN00072985'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%BORGACCI%' and FirstName like '%DAVIDE%'and Id = 'WGS\WN00071358'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%PIZZICO%' and FirstName like '%ANDREA%'and Id = 'WGS\WN00072785'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%SANTORO%' and FirstName like '%GUGLIELMO%'and Id = 'WGS\WN00073020'
--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%SAPUCCI%' and FirstName like '%GIOVANNI%'and Id = 'WGS\WN00073026'

