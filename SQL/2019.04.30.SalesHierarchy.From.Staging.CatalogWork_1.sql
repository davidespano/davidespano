--By Logging the usual user POZZOBON WGS\\WN00106285 with Postman on DEV (SL-Cloud-Catalog-DEV) 
--SLCatalogDbDev
--Once you have looged in you can find your session like this
select top 100 * from cat.[Session] order by Expiry desc

--Once you are authenticated a session is created for the user in the Session Table
--The session holds a Bearer Token that is returned to the logged in user in the headres of the response
--You can see the Bearer Token in the Headers tab of Postman for the entry Authorization
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Token																																						AuthUserId		AppUserId		Expiry					DatabaseConnection																																		TenantId	ApplicationId		ApplicationType
--NRMqzusOmgB2ILIPVrkicV8Yhq7GS7dOprkQMiyqw8hCE4DW5Aq79EddCi36CwtYJ6AaveTsy4SnSxRbefpzr5Mxa3/OwFAx81o6PDilhTipNQjIk7+waRndtsL4wd+2d5/ednScBLkTh6S/vX9bIg==	WGS\WN00106285	WGS\WN00106285	2019-04-30 12:55:09.213	MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SL_PHXDEV_DEV;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD	PHXDEV		speedylead_phx_dev	speedylead
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--cat.UserTenantRelation
select * from cat.UserTenantRelation where UserId = 'WGS\WN00106285'
--This must be modified
--The AppUserId must be set to the value of SL_PHXDEV_DEV.sf.SalesForceEntity.Id 
--for the match on at.UserTenantRelation.UserId =  SL_PHXDEV_DEV.sf.SalesForceEntity.SystemAccount
------------------------------------------------------------------------------
--UserId			TenantId	ApplicationType	AppUserId	ApplicationId
--WGS\WN00106285	PHXDEV		speedylead		NULL		NULL
--WGS\WN00106285	PHXPROD		speedylead		NULL		NULL
--WGS\WN00106285	PHXTEST		speedylead		NULL		NULL
------------------------------------------------------------------------------

--From the sf.SalesForceEntity of SL_PHXDEV_DEV we get the new AppUserId as the Id = 14200
------------------------------------------------------------------------------------------------------------------------------------------------------------
--RowStatus	RowCreationTime				RowModificationTime	Id		FirstName	LastName	SystemAccount	Status	ParentId	Level
--1			2019-04-30 13:31:05.9633333	NULL				14200	FABIO 		POZZOBON	WGS\WN00106285	0		3658		3
------------------------------------------------------------------------------------------------------------------------------------------------------------
update cat.UserTenantRelation set AppUserId = '14200' where UserId = 'WGS\WN00106285' and TenantId = 'PHXDEV'
select * from cat.UserTenantRelation where UserId = 'WGS\WN00106285'
------------------------------------------------------------------------------
--UserId			TenantId	ApplicationType	AppUserId	ApplicationId
--WGS\WN00106285	PHXDEV		speedylead		14200		NULL
--WGS\WN00106285	PHXPROD		speedylead		NULL		NULL
--WGS\WN00106285	PHXTEST		speedylead		NULL		NULL
------------------------------------------------------------------------------
--reverse POZZOBON
update cat.UserTenantRelation set AppUserId = null where UserId = 'WGS\WN00106285' and TenantId = 'PHXDEV'
select * from cat.UserTenantRelation where UserId = 'WGS\WN00106285'