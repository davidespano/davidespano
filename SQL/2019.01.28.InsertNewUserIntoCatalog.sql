﻿-------------------------------------  Example -------------------------------------------------------------------
--INSERT INTO [cat].[User] 
--(Id, FirstName, LastName, Password, PasswordSalt, PasswordIteration) 
--VALUES ('WP\psl0001','Alan','Ford', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'bobrock' + 'salt1')), 'salt1', 0 )
--GO
------------------------------------------------------------------------------------------------------------------

--Insert New Pilot User
--Patrik Lanthaler
insert into SLCatalogDb.cat.[User] 
(Id, AuthUserName, [Role], FirstName, LastName, PhoneNumber, [Language], Email, [Password], Photo)
values
('WGS\WN00072170', 'WGS\WN00072170','NormalUser', 'Patrick', 'Lanthaler', null, null, null, HASHBYTES('MD5',CONVERT(NVARCHAR(50),'pl190128' + 'salt')), null)

--User Tenat Relation
insert  into SLCatalogDb.cat.UserTenantRelation
(UserId, TenantId)
values
('WGS\WN00072170','WIT')

--Insert New Pilot User
--Federico Libera
insert into SLCatalogDb.cat.[User] 
(Id, AuthUserName, [Role], FirstName, LastName, PhoneNumber, [Language], Email, [Password], Photo)
values
('WGS\WN00073509', 'WGS\WN00073509', 'NormalUser', 'Federico', 'Libera', null, null, null, HASHBYTES('MD5',CONVERT(NVARCHAR(50),'fl190128' + 'salt')), null)

--User Tenat Relation
insert  into SLCatalogDb.cat.UserTenantRelation
(UserId, TenantId)
values
('WGS\WN00073509','WIT')