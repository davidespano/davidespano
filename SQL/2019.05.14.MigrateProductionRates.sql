
--------------------------------------------------------------------------------------------------------
--This is the migration 20181210143919_rate_0 from 20181210143919_SpeedyLead.Module.Rate.Data.Ef_0.sql
--which creates the nes namespace 'rat' and in it the table Rate.

--Commit 8daf237e Davide Spano <Davide.Spano@wuerth-phoenix.com> 1/5/2019 8:03:37 PM
--------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181210143919_rate_0')
BEGIN
    IF SCHEMA_ID(N'rat') IS NULL EXEC(N'CREATE SCHEMA [rat];');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181210143919_rate_0')
BEGIN
    CREATE TABLE [rat].[Rate] (
        [RowStatus] tinyint NOT NULL,
        [RowCreationTime] datetime2 NOT NULL,
        [RowModificationTime] datetime2 NULL,
        [Id] int NOT NULL IDENTITY,
        [Identifier] varchar(250) NOT NULL,
        [Value] int NOT NULL,
        [EventId] int NOT NULL,
        CONSTRAINT [PK_Rate] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181210143919_rate_0')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20181210143919_rate_0', N'2.2.0-rtm-35687');
END;

GO

--------------------------------------------------------------------------------------------------------
--Commit 9819ef44 Davide Spano <Davide.Spano@wuerth-phoenix.com> 12/13/2018 1:26:04 PM
--replaced Feedback module with Rate module and renamed all folders to *.Rate.* where appropriate + added Utils prohject, changed solution file.

--The script above creates the [rat].[Rate] table

--------------------------------------------------
 --CREATE TABLE [rat].[Rate] (
 --       [RowStatus] tinyint NOT NULL,
 --       [RowCreationTime] datetime2 NOT NULL,
 --       [RowModificationTime] datetime2 NULL,
 --       [Id] int NOT NULL IDENTITY,
 --       [Identifier] varchar(250) NOT NULL,
 --       [Value] int NOT NULL,
 --       [EventId] int NOT NULL,
 --       CONSTRAINT [PK_Rate] PRIMARY KEY ([Id])
 --   );
 --------------------------------------------------

 --However in production I have two tables below
 
  --------------------------------------------------
-- CREATE TABLE [raf].[RideAlongFeedback](
--	[RowStatus] [tinyint] NOT NULL,
--	[RowCreationTime] [datetime2](7) NOT NULL,
--	[RowModificationTime] [datetime2](7) NULL,
--	[Id] [int] IDENTITY(1,1) NOT NULL,
--	[RideAlongId] [int] NOT NULL,
--	[Status] [tinyint] NOT NULL,
-- CONSTRAINT [PK_RideAlongFeedback] PRIMARY KEY CLUSTERED 
--(
--	[Id] ASC
--)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
--) ON [PRIMARY]
--GO
 --------------------------------------------------
-- CREATE TABLE [raf].[RideAlongFeedbackRateValue](
--	[RowStatus] [tinyint] NOT NULL,
--	[RowCreationTime] [datetime2](7) NOT NULL,
--	[RowModificationTime] [datetime2](7) NULL,
--	[Id] [int] IDENTITY(1,1) NOT NULL,
--	[Identifier] [varchar](250) NOT NULL,
--	[Value] [int] NOT NULL,
--	[RideAlongFeedbackId] [int] NOT NULL,
-- CONSTRAINT [PK_RideAlongFeedbackRateValue] PRIMARY KEY CLUSTERED 
--(
--	[Id] ASC
--)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--ALTER TABLE [raf].[RideAlongFeedbackRateValue]  WITH CHECK ADD  CONSTRAINT [FK_RideAlongFeedbackRateValue_RideAlongFeedback_RideAlongFeedbackId] FOREIGN KEY([RideAlongFeedbackId])
--REFERENCES [raf].[RideAlongFeedback] ([Id])
--ON DELETE CASCADE
--GO

--ALTER TABLE [raf].[RideAlongFeedbackRateValue] CHECK CONSTRAINT [FK_RideAlongFeedbackRateValue_RideAlongFeedback_RideAlongFeedbackId]
--GO
 --------------------------------------------------

 --If we compare [raf].[RideAlongFeedbackRateValue] with [rat].[Rate] we see they have basically the same structure
 --the [raf].[RideAlongFeedbackRateValue].[RideAlongFeedbackId] has a FK contraint to the [raf].[RideAlongFeedback].[Id]
 --while the [raf].[RideAlongFeedback].[RideAlongId] points to the [cal].[RideAlong]
 
--------------------------------------------------
 --CREATE TABLE [rat].[Rate] (
 --       [RowStatus] tinyint NOT NULL,
 --       [RowCreationTime] datetime2 NOT NULL,
 --       [RowModificationTime] datetime2 NULL,
 --       [Id] int NOT NULL IDENTITY,
 --       [Identifier] varchar(250) NOT NULL,
 --       [Value] int NOT NULL,
 --       [EventId] int NOT NULL,
 --       CONSTRAINT [PK_Rate] PRIMARY KEY ([Id])
 --   );
 --------------------------------------------------

 --What we need to do 

 --1 first run the migration that creates [rat].[Rate]
 --2 then the data in the table [raf].[RideAlongFeedbackRateValue] must be copies over to [rat].[Rate]*
 --3 the [raf].[RideAlongFeedbackRateValue] and [raf].[RideAlongFeedback] can be dropped


 --------------------------------
 --Step 2 is the most important
 --------------------------------

 --We must map [raf].[RideAlongFeedbackRateValue].[RideAlongFeedbackId] to [rat].[Rate].[EventId] 
 --that is replace the former with the latter while copying the data.