CREATE USER SpeedyLeadUser WITH PASSWORD = 'S733dy43aD', DEFAULT_SCHEMA=[dbo]

sp_addrolemember 'db_datareader', 'SpeedyLeadUser';
GO
sp_addrolemember 'db_datawriter', 'SpeedyLeadUser';
GO
sp_addrolemember 'db_owner', 'SpeedyLeadUser';