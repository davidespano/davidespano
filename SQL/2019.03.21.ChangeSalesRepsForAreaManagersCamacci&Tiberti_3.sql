
--Remove these
--==========================================================================================================
--WGS\WN00071527
--CAMACCI, STEFANO	11757	BORGACCI, FRANCESCO	ENASARCO FORZA VENDITA	13.10.2008	HW	ME		--WGS\WN00071359
--CAMACCI, STEFANO	12387	QUERO, MARIO	DIPENDENTE FORZA VENDITA	21.05.2010	HW	ME		--WGS\WN00072685
--CAMACCI, STEFANO	13593	BRUNETTI, PAOLO	ENASARCO FORZA VENDITA	08.06.2015	HW	ME			--WGS\WN00084114
--CAMACCI, STEFANO	13719	D'ANIELLO, RAFFAELE	ENASARCO FORZA VENDITA	09.11.2015	HW	ME		--WGS\WN00088522
--CAMACCI, STEFANO	14010	SORICETTI, LUCA	ENASARCO FORZA VENDITA	27.10.2016	HW	ME			--WGS\WN00099857
--CAMACCI, STEFANO	14244	MANCINELLI, MATTEO	ENASARCO FORZA VENDITA	01.09.2017	HW	ME		--WGS\WN00111934
--CAMACCI, STEFANO	14799	FEDELI, ALESSANDRO	ENASARCO FORZA VENDITA	12.11.2018	HW	ME		--WGS\WN00128930
--CAMACCI, STEFANO	7072	FELICI, ERMELINDO	ENASARCO FORZA VENDITA	23.08.2004	HW	ME		--WGS\WN00071880
--CAMACCI, STEFANO	8471	CLEMENTONI, MAURIZIO	ENASARCO FORZA VENDITA	21.10.1997	HW	ME	--WGS\WN00071508
--==========================================================================================================
--WGS\WN00073088
--TIBERTI, FRANCESCO	10665	LONGARINI, NICOLA	DIPENDENTE FORZA VENDITA	16.01.2006	SO	EL		--WGS\WN00072165   
--TIBERTI, FRANCESCO	13272	D'AGOSTINO, LUIGI	ENASARCO FORZA VENDITA	15.04.2002	PO	CO			--WGS\WN00073088
--TIBERTI, FRANCESCO	9157	GABRIELLI, TIZIANO	ENASARCO FORZA VENDITA	17.07.1998	PO	EL			--WGS\WN00071981
----------------------------------------------------------------
--Please further remove from the area of Tiberti Francesco:
--Stocchi Massimo Matr. 3989																			--WGS\WN00073064
--D�Agostino Luigi Matr. 13272																			--WGS\WN00073088  (2)
----------------------------------------------------------------
--And add to his area:
--Fiorini Michele Matr. 14932 (To set to status 1)														--?
--==========================================================================================================

--Please delete the sales reps in the attached file of the areas of Camacci and Tiberti.
--select * from sf.[User] where Id = 'WGS\WN00073088'  --TIBERTI
--select * from sf.SalesRep where UserId = 'WGS\WN00073088' order by LastName
--TIBERTI, FRANCESCO --WGS\WN00073088
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00073088' and Id = 'WGS\WN00076726' and LastName like '%D%AGOSTINO%' and FirstName like '%LUIGI%'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00073088' and Id = 'WGS\WN00073064' and LastName like 'STOCCHI' and FirstName like 'MASSIMO'
-------------------------------------------------------------------------------------------------------------------------------------------------------------
--These two are actually already assigned to CAMACCI.
--select * from sf.SalesRep where LastName like 'LONGARINI'
--select * from sf.SalesRep where LastName like 'GABRIELLI'
--update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00073088' and Id = 'WGS\WN00072165' and LastName like 'LONGARINI' and FirstName like 'NICOLA'
--update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00073088' and Id = 'WGS\WN00071981' and LastName like 'GABRIELLI' and FirstName like 'TIZIANO'
-------------------------------------------------------------------------------------------------------------------------------------------------------------

-- 
--select * from sf.SalesRep where LastName like 'FIORINI'
--select * from sf.SalesRep where Id = 'WGS\WN00132506'
--delete from sf.SalesRep where Id = 'WGS\WN00132506'
declare @ts datetime
set @ts = GETDATE()
--select @ts
insert into sf.SalesRep (RowStatus,RowCreationTime,Id,FirstName,LastName,UserId,[Status]) values (1,@ts,'WGS\WN00132506','MICHELE','FIORINI','WGS\WN00073088',1)


--select * from sf.SalesRep where LastName like 'QUERO'
----
--select * from sf.[User] where Id = 'WGS\WN00071527'  --CAMACCI
--select * from sf.SalesRep where UserId = 'WGS\WN00071527' order by LastName
--CAMACCI, STEFANO ----WGS\WN00071527
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00071359' and LastName like 'BORGACCI' and FirstName like 'FRANCESCO'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00072685' and LastName like 'QUERO' and FirstName like 'MARIO'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00084114' and LastName like 'BRUNETTI' and FirstName like 'PAOLO'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00088522' and LastName like 'D%ANIELLO' and FirstName like 'RAFFAELE'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00099857' and LastName like 'SORICETTI' and FirstName like 'LUCA'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00111934' and LastName like 'MANCINELLI' and FirstName like 'MATTEO'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00128930' and LastName like 'FEDELI' and FirstName like 'ALESSANDRO'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00071880' and LastName like 'FELICI' and FirstName like 'ERMELINDO'
update sf.SalesRep set UserId = NULL where UserId = 'WGS\WN00071527' and Id = 'WGS\WN00071508' and LastName like 'CLEMENTONI' and FirstName like 'MAURIZIO'



