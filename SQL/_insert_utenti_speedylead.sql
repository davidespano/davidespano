INSERT INTO [cat].[Tenant] (Id, Name, ShortName,AuthUrl,AuthMode) VALUES ('WIT','Würth Italia S.r.l.', 'Würth Italia','https://','Basic') 
GO

INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) 
VALUES ('WGS\WN00071525','WGS\WN00071525','Ivan','Calvini', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'ic617876' + 'salt')) )

INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) 
VALUES ('WGS\WN00071375','WGS\WN00071375','Dario','Bruno', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'db223464' + 'salt')) )

INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) 
VALUES ('WGS\WN00073088','WGS\WN00073088','Francesco','Tiberti', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'ft542876' + 'salt')) )

INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) 
VALUES ('WGS\WN00071527','WGS\WN00071527','Stefano','Camacci', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'sc435687' + 'salt')) )

-- IT test user
INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) 
VALUES ('WGS\WN00072118','WGS\WN00072118','WIT','Throubleshooter', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'wt156383' + 'salt')) ) 

GO

INSERT INTO [cat].[ApplicationServer] (Id, Name, TenantId, Url, DbConnection, Version, DbVersion) 
VALUES ('speedylead', 'Speedy Lead', 'WIT','https://sl100000.azurewebsites.net/api/v1', 
'MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SL_WIT_PROD;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD', '00.00.02.0000', '00.00.01') 
GO

-- test
INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WGS\WN00071525', 'WIT') 
INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WGS\WN00071375', 'WIT') 
INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WGS\WN00073088', 'WIT') 
INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WGS\WN00071527', 'WIT') 
INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WGS\WN00072118', 'WIT') 

GO

/*
INSERT INTO [cat].[Tenant] (Id, Name, ShortName, AuthUrl, AuthMode) VALUES ('WP','Würth Phoenix S.r.l.','Würth Phoenix','https://speedytouchdemowp.wuerth-phoenix.com/TokenService/TokenService.svc/gettokenstring','Bearer') 
GO
INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) VALUES ('WGS\WN00106285','WP\pxsl00001','Donkey','Kong', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'abc123' + 'salt')) )
GO
INSERT INTO [cat].[ApplicationServer] (Id, Name, TenantId, Url, DbConnection, Version, DbVersion) VALUES ('sl000001', 'speedylead', 'WP','https://sl000001.azurewebsites.net/api/v1', 'MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SLDemoTenant;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD', '00.00.02.0000', '00.00.01') 
GO

*/

-- WP TEST
INSERT INTO [cat].[Tenant] ([Id], [Name], [ShortName], [AuthUrl], [AuthMode]) VALUES ('WPT','Demo Würth Phoenix S.r.l.', 'WP Demo','https://speedytouchdemowp.wuerth-phoenix.com/TokenService/TokenService.svc/gettokenstring','Basic') 
GO
INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) VALUES ('WP\psl0001','WP\psl0001','Alan','Ford', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'bobrock' + 'salt')) )
GO
INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) VALUES ('WP\psl0002','WP\psl0002','Throubleshooting','User2', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'bobrock' + 'salt')) )
GO
INSERT INTO [cat].[User] (Id, AuthUserName, FirstName, LastName, Password) VALUES ('WP\psl0003','WP\psl0003','Throubleshooting','User3', HASHBYTES('MD5',CONVERT(NVARCHAR(50),'bobrock' + 'salt')) )
GO

-- POZZOBON 'WGS\WN00106285' 
INSERT INTO [cat].[ApplicationServer] (Id, Name, TenantId, Url, DbConnection, Version, DbVersion) 
VALUES ('speedylead', 'Speedy Lead', 'WPT','https://sl000001.azurewebsites.net/api/v1', 
'MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SLDemoTenant;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD', '00.00.02.0000', '00.00.01') 
GO

INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WP\psl0001', 'WPT') 
GO

INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WP\psl0002', 'WPT') 
GO

INSERT INTO [cat].[UserTenantRelation] (UserId,TenantId) VALUES ('WP\psl0003', 'WPT') 
GO

--------------------------
-- UPDATE [cat].[User] set Password=HASHBYTES('MD5',CONVERT(NVARCHAR(50),'wt156383' + 'salt')) where id = 'WGS\WN00072118';
-- UPDATE [cat].[User] set Id='WGS\WN00106285' where id = 'WP\psl0001';
-- UPDATE [cat].[UserTenantRelation] set TenantId='WPT' where UserId = 'WGS\WN00106285' and TenantId='WIT' ;

-- UPDATE [cat].[ApplicationServer] set DbConnection='MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SL_WIT_PROD;Integrated Security=False;User ID=SpeedyLead_WIT;Password=dagub2.3s657d' where TenantId = 'WIT'  ;
-- UPDATE [cat].[ApplicationServer] set Url='https://sl100000.azurewebsites.net/api/v1' where TenantId = 'WIT';
-- UPDATE [cat].[ApplicationServer] set Version='01.00.00.0000' where TenantId = 'WIT' and Id='speedylead';
-- UPDATE [cat].[ApplicationServer] set DbVersion='01.00.00.0000' where TenantId = 'WIT' and Id='speedylead';
