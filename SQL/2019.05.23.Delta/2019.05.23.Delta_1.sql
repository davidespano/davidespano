--check area manager all exists already
select * from sf.SalesForceEntity where Id in 
(
'13987',
'10301',
'2973',
'10563',
'6047',
'13115'
)

--deletes
select * from sf.SalesForceEntity where Id in 
(
'10742',
'59484',
'11757',
'13593',
'8471',
'13719',
'14799',
'7072',
'14244',
'12387',
'14010',
'13612',
'14128',
'13272',
'9157',
'10665',
'3989',
'14609',
'14787',
'59495'
)

--These are all the deletes
--delete from sf.SalesForceEntity where Id = '10742'
--delete from sf.SalesForceEntity where Id = '59484'
--delete from sf.SalesForceEntity where Id = '11757'
--delete from sf.SalesForceEntity where Id = '13593'
--delete from sf.SalesForceEntity where Id = '8471'
--delete from sf.SalesForceEntity where Id = '13719'
--delete from sf.SalesForceEntity where Id = '14799'
--delete from sf.SalesForceEntity where Id = '7072'
--delete from sf.SalesForceEntity where Id = '14244'
--delete from sf.SalesForceEntity where Id = '12387'
--delete from sf.SalesForceEntity where Id = '14010'
--delete from sf.SalesForceEntity where Id = '13612'
--delete from sf.SalesForceEntity where Id = '14128'
--delete from sf.SalesForceEntity where Id = '13272'
--delete from sf.SalesForceEntity where Id = '9157'
--delete from sf.SalesForceEntity where Id = '10665'
--delete from sf.SalesForceEntity where Id = '3989'
--delete from sf.SalesForceEntity where Id = '14609'
--delete from sf.SalesForceEntity where Id = '14787'
--delete from sf.SalesForceEntity where Id = '59495'

--inserts
select RowStatus,Id,FirstName,LastName,SystemAccount,ParentId,Level,SalesRepStatus from sf.SalesForceEntity where Id in 
(
'14966',
'14806',--
'15058',
'9157',--
'10665',--
'3907',--
'6119',--
'15044',
'3282',--
'14932',
'14963'
)

--These already exist so need an update
--------------------------------------------------------------------------------------------------------------
--RowStatus	Id		FirstName		LastName	SystemAccount	ParentId	Level	SalesRepStatus
--1			10665	NICOLA			LONGARINI	WGS\WN00072165	6047		4		2
--1			14806	FRANCESCO		AMADORI		WGS\WN00128932	2371		4		1
--1			3282	FRANCESCO		FACCANI		WGS\WN00071866	3833		4		4
--1			3907	ALBERTO			PRIORI		WGS\WN00072731	8538		4		2
--1			6119	DANIEL PABLO	SPADONI		WGS\WN00073027	2371		4		3
--1			9157	TIZIANO			GABRIELLI	WGS\WN00071981	6047		4		2
--------------------------------------------------------------------------------------------------------------
declare @ts datetime
set @ts = GETDATE()

update sf.SalesForceEntity set ParentId='2973', RowModificationTime = @ts  where Id = '14806' and ParentId = '2371'
update sf.SalesForceEntity set ParentId='2973', RowModificationTime = @ts  where Id = '9157' and ParentId = '6047'
update sf.SalesForceEntity set ParentId='2973', RowModificationTime = @ts where Id = '10665' and ParentId = '6047'
update sf.SalesForceEntity set ParentId='2973', RowModificationTime = @ts where Id ='3907' and ParentId = '8538'
update sf.SalesForceEntity set ParentId='2973', RowModificationTime = @ts where Id = '6119'and ParentId = '2371'
update sf.SalesForceEntity set ParentId='6047', RowModificationTime = @ts  where Id = '3282' and ParentId = '3833'

--new inserts
--inserts
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id.ParentId,SystemAccount,FirstName,LastName,[Level],SalesRepStatus) 
values (1,@ts,'14966','13987','WGS\WN00135021','MATTIA','MACOBATTI',4,1)

insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id.ParentId,SystemAccount,FirstName,LastName,[Level],SalesRepStatus) 
values (1,@ts,'15058','2973','WGS\WN00137831','LORENZO','AMICUCCI',4,1)


insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id.ParentId,SystemAccount,FirstName,LastName,[Level],SalesRepStatus) 
values (1,@ts,'15044','10563','WGS\WN00137136','ALEXANDER','BRUNNER',4,1)


insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id.ParentId,SystemAccount,FirstName,LastName,[Level],SalesRepStatus) 
values (1,@ts,'14932','6047','WGS\WN00132506','MICHELE','FIORINI',4,1)


insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id.ParentId,SystemAccount,FirstName,LastName,[Level],SalesRepStatus) 
values (1,@ts,'14963','13115','WGS\WN000135025','MATTEO','LORENZI',4,1)




