--https://stackoverflow.com/questions/7469130/cannot-drop-database-because-it-is-currently-in-use
--This will tell you which processes are using it.
select * from sys.sysprocesses where dbid = DB_ID('SLDemoTenant')

-- to force drop then, the ultimate approach is
--USE master;
--GO
--ALTER DATABASE SLDemoTenant 
--SET SINGLE_USER 
--WITH ROLLBACK IMMEDIATE;
--GO
--DROP DATABASE SLDemoTenant
