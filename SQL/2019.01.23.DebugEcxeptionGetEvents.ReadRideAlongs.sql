--------------------------------------------------------------------------------
--To Connect to the Azure SQL Server : bg6arbmxb8.database.windows.net

--usr:SpeedyLeadUser
--S733dy43aD

--usr:SpeedyAdmin
--An9acer1dirB
--------------------------------------------------------------------------------

--used by W�rth-IT pilots and later production
use SL_WIT_PROD
go

--this should be a replica of what in production i.e. SL_WIT_PROD
use SL_PHXPROD_PROD
go

--this should be tthe target of the end-of-sprint release tested by Tilo
use SL_PHXTEST_TEST
go

--This should be target during the sprint
use SL_PHXDEV_DEV
go

--This is old and played the role of SL_PHXDEV_DEV but will be scrapped soon
use SLDemoTenant
go

select * from cal.RideAlong order by RowCreationTime desc

--------------------------------------------------------------------------------
--The test user who interacts with the data in SL_PHXDEV_DEV or SL_PHXTEST_TEST
--logs into SpeedyLead client with the following credentials:
--tenant: WPT
--user: WGS\WN00106285
--password: abc123
--------------------------------------------------------------------------------

select * from cal.RideAlong where [User] like '%WGS\WN00106285%' order by RowCreationTime desc