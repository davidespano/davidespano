﻿--this script must be run on 
--Server: bg6arbmxb8.database.windows.net 
--Catalog: SLCatalogDbDev

--For the tenantId=PHXDEV on SLCatalogDbDev we update from
--MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SLDemoTenant;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD
DECLARE @phxdevcs nvarchar(2048)
SET @phxdevcs = 'MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SL_PHXDEV_DEV;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD'
UPDATE [cat].[ApplicationServerInstance] 
set DbConnection= @phxdevcs
where TenantId = 'PHXDEV' ;

--For the tenant PHXTEST on SLCatalogDbDev we update from
--MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SL_PHXTEST_TEST;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD
--UPDATE [cat].[ApplicationServerInstance] 
--set DbConnection=‘MSSQL|Data Source=bg6arbmxb8.database.windows.net;Initial Catalog=SL_PHXTEST_TEST;Integrated Security=False;User ID=SpeedyLeadUser;Password=S733dy43aD’ 
--where TenantId = ‘PHXTEST’ ;