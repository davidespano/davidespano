
--=======================================================================================================================================================
--This is the exported Schema definition of the SH table in staging

/****** Object:  Table [dbo].[SalesHierarchy]    Script Date: 4/24/2019 9:28:13 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[SalesHierarchy](
--	[Id] [nvarchar](450) NOT NULL,
--	[ParentId] [nvarchar](450) NULL,
--	[SystemAccount] [nvarchar](450) NOT NULL,
--	[FirstName] [nvarchar](100) NOT NULL,
--	[LastName] [nvarchar](100) NOT NULL,
--	[ZipCode] [nvarchar](10) NOT NULL,
--	[IsoCountryCode] [nvarchar](35) NULL,
--	[City] [nvarchar](35) NULL,
--	[Street] [nvarchar](35) NULL,
--	[Mobile] [nvarchar](35) NULL,
--	[SalesChannel] [nvarchar](2) NULL,
--	[Division] [nvarchar](2) NULL,
--	[EmployedSince] [date] NULL,
--	[Level] [smallint] NOT NULL,
--	[Status] [smallint] NULL,
--	[Company] [nvarchar](4) NOT NULL,
-- CONSTRAINT [PK_SalesHierarchy] PRIMARY KEY NONCLUSTERED 
--(
--	[Id] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO


--=======================================================================================================================================================

--update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%TANCREDI%' and FirstName like '%PIERO%'and Id = 'WGS\WN00073104'
--insert into sf.SalesRep (RowStatus,RowCreationTime,Id,FirstName,LastName,UserId) values (1,@ts,'WGS\WN00128932','AMADORI','FRANCESCO','WGS\WN00071527')

--Insert the user that is used when you debug into the new SH table
-- WGS\\WN00106285 POZZOBON
--14200	3658	WGS\WN00106285	FABIO	POZZOBON	4	0
--INSERTINTO,@14200XX@,@3658@,@WGS\WN00106285@,@FABIO@,@POZZOBON@,4,0**
--INSERTINTO,14200','3658','WGS\WN00106285','FABIO','POZZOBON',4,,0,,symbolz
declare @ts datetime
set @ts = GETDATE()
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,);


--=======================================================================================================================================================
--Token Replacements

--1
--INSERTINTO,
--insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'

--2
--,symboly,
--','

--3
--,symbolx,
--',

--4
--,,symbolz
--);
--=======================================================================================================================================================
--This should fail as it violates the FK constraint as the ParentId=3658 should be inserted first
declare @ts datetime
set @ts = GETDATE()
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'14200','3658','WGS\WN00106285','FABIO','POZZOBON',4,0);

--This should as we have set the FK constraint as the ParentId=NULL
declare @ts datetime
set @ts = GETDATE()
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'14200',NULL,'WGS\WN00106285','FABIO','POZZOBON',4,0);

--verify & clean up
select * from sf.SalesForceEntity
delete from sf.SalesForceEntity
--=======================================================================================================================================================

--This is the hierarchy for Pozzobon 14200 WGS\WN00106285 as it is in the CSV
INSERTINTO	13670	symboly		    symboly	WGS\WN00087100	symboly	CHRISTOPH	symboly	LADURNER				symbolx	2	0	,symbolz
INSERTINTO	3658	symboly	13670	symboly	WGS\WN00069099	symboly	SERGIO		symboly	SMANIOTTO				symbolx	3	0	,symbolz
INSERTINTO	14200	symboly	3658	symboly	WGS\WN00106285	symboly	FABIO		symboly	POZZOBON				symbolx	4	0	,symbolz
INSERTINTO	14689	symboly	14200	symboly	WGS\WN00125448	symboly	GHERARDO	symboly	BELLONI					symbolx	5	0	,symbolz
INSERTINTO	4023	symboly	14200	symboly	WGS\WN00071404	symboly	MAURIZIO	symboly	BERTOLI					symbolx	5	0	,symbolz
INSERTINTO	1208	symboly	14200	symboly	WGS\WN00071491	symboly	ELIA		symboly	CHIES					symbolx	5	0	,symbolz
INSERTINTO	10112	symboly	14200	symboly	WGS\WN00071566	symboly	JOSEPH		symboly	CONTE					symbolx	5	0	,symbolz
INSERTINTO	13871	symboly	14200	symboly	WGS\WN00094079	symboly	MATTEO		symboly	DAL CIN					symbolx	5	0	,symbolz
INSERTINTO	7122	symboly	14200	symboly	WGS\WN00071832	symboly	PAOLO		symboly	DE PASCALI				symbolx	5	0	,symbolz
INSERTINTO	3198	symboly	14200	symboly	WGS\WN00071911	symboly	MARCO		symboly	FONTANINI				symbolx	5	0	,symbolz
INSERTINTO	6160	symboly	14200	symboly	WGS\WN00072027	symboly	FRANCESCO	symboly	GENERO					symbolx	5	0	,symbolz
INSERTINTO	14042	symboly	14200	symboly	WGS\WN00100884	symboly	LUCA		symboly	MATURI					symbolx	5	0	,symbolz
INSERTINTO	5465	symboly	14200	symboly	WGS\WN00072434	symboly	ENZO		symboly	MEZZAVILLA				symbolx	5	0	,symbolz
INSERTINTO	14625	symboly	14200	symboly	WGS\WN00121862	symboly	MAURO		symboly	PAVAN					symbolx	5	0	,symbolz
INSERTINTO	8711	symboly	14200	symboly	WGS\WN00072570	symboly	RALF		symboly	PELLIZZARI				symbolx	5	0	,symbolz
INSERTINTO	4843	symboly	14200	symboly	WGS\WN00072984	symboly	SANDRO		symboly	SKARABOT				symbolx	5	0	,symbolz
INSERTINTO	14499	symboly	14200	symboly	WGS\WN00118825	symboly	SIMONE		symboly	STUDIO DOGE DI STANCHI	symbolx	5	0	,symbolz

--This is the version with the root node added 
INSERTINTO  ROOTID  symboly null    symboly WGSROOT         symboly ROOTFIRSTNAME   symboly ROOTLASTNAME        symbolx 1   0   ,symbolz
INSERTINTO	13670	symboly	ROOTID	symboly	WGS\WN00087100	symboly	CHRISTOPH	symboly	LADURNER				symbolx	2	0	,symbolz
INSERTINTO	3658	symboly	13670	symboly	WGS\WN00069099	symboly	SERGIO		symboly	SMANIOTTO				symbolx	3	0	,symbolz
INSERTINTO	14200	symboly	3658	symboly	WGS\WN00106285	symboly	FABIO		symboly	POZZOBON				symbolx	4	0	,symbolz
INSERTINTO	14689	symboly	14200	symboly	WGS\WN00125448	symboly	GHERARDO	symboly	BELLONI					symbolx	5	0	,symbolz
INSERTINTO	4023	symboly	14200	symboly	WGS\WN00071404	symboly	MAURIZIO	symboly	BERTOLI					symbolx	5	0	,symbolz
INSERTINTO	1208	symboly	14200	symboly	WGS\WN00071491	symboly	ELIA		symboly	CHIES					symbolx	5	0	,symbolz
INSERTINTO	10112	symboly	14200	symboly	WGS\WN00071566	symboly	JOSEPH		symboly	CONTE					symbolx	5	0	,symbolz
INSERTINTO	13871	symboly	14200	symboly	WGS\WN00094079	symboly	MATTEO		symboly	DAL CIN					symbolx	5	0	,symbolz
INSERTINTO	7122	symboly	14200	symboly	WGS\WN00071832	symboly	PAOLO		symboly	DE PASCALI				symbolx	5	0	,symbolz
INSERTINTO	3198	symboly	14200	symboly	WGS\WN00071911	symboly	MARCO		symboly	FONTANINI				symbolx	5	0	,symbolz
INSERTINTO	6160	symboly	14200	symboly	WGS\WN00072027	symboly	FRANCESCO	symboly	GENERO					symbolx	5	0	,symbolz
INSERTINTO	14042	symboly	14200	symboly	WGS\WN00100884	symboly	LUCA		symboly	MATURI					symbolx	5	0	,symbolz
INSERTINTO	5465	symboly	14200	symboly	WGS\WN00072434	symboly	ENZO		symboly	MEZZAVILLA				symbolx	5	0	,symbolz
INSERTINTO	14625	symboly	14200	symboly	WGS\WN00121862	symboly	MAURO		symboly	PAVAN					symbolx	5	0	,symbolz
INSERTINTO	8711	symboly	14200	symboly	WGS\WN00072570	symboly	RALF		symboly	PELLIZZARI				symbolx	5	0	,symbolz
INSERTINTO	4843	symboly	14200	symboly	WGS\WN00072984	symboly	SANDRO		symboly	SKARABOT				symbolx	5	0	,symbolz
INSERTINTO	14499	symboly	14200	symboly	WGS\WN00118825	symboly	SIMONE		symboly	STUDIO DOGE DI STANCHI	symbolx	5	0	,symbolz

--=======================================================================================================================================================
--Tranformed for the insert
INSERTINTO,ROOTID,symboly,null,symboly,WGSROOT,symboly, ROOTFIRSTNAME,symboly,ROOTLASTNAME,symbolx,1,0,symbolz
INSERTINTO,13670,symboly,ROOTID,symboly,WGS\WN00087100,symboly,CHRISTOPH,symboly,LADURNER,symbolx,2,0,symbolz
INSERTINTO,3658,symboly,13670,symboly,WGS\WN00069099,symboly,SERGIO	,symboly,SMANIOTTO,symbolx,3,0,symbolz
INSERTINTO,14200,symboly,3658,symboly,WGS\WN00106285,symboly,FABIO	,symboly,POZZOBON,symbolx,4,0,symbolz
INSERTINTO,14689,symboly,14200,symboly,WGS\WN00125448,symboly,GHERARDO,symboly,BELLONI,symbolx,5,0,symbolz
INSERTINTO,4023,symboly,14200,symboly,WGS\WN00071404,symboly,MAURIZIO,symboly,BERTOLI,symbolx,5,0,symbolz
INSERTINTO,1208,symboly,14200,symboly,WGS\WN00071491,symboly,ELIA,symboly,CHIES,symbolx,5,0,symbolz
INSERTINTO,10112,symboly,14200	symboly,WGS\WN00071566,symboly,JOSEPH,symboly,CONTE,symbolx,5,0,symbolz
INSERTINTO,13871,symboly,14200,symboly,WGS\WN00094079,symboly,MATTEO,symboly,DAL CIN,symbolx,5,0,symbolz
INSERTINTO,7122,symboly,14200,symboly,WGS\WN00071832,symboly,PAOLO,symboly,DE PASCALI,symbolx,5,0,symbolz
INSERTINTO,3198,symboly,14200,symboly,WGS\WN00071911,symboly,MARCO,symboly,FONTANINI,symbolx,5,0,symbolz
INSERTINTO,6160,symboly,14200,symboly,WGS\WN00072027,symboly,FRANCESCO,symboly,GENERO,symbolx,5,0,symbolz
INSERTINTO,14042,symboly,14200,symboly,WGS\WN00100884,symboly,LUCA,symboly,MATURI,symbolx,5,0,symbolz
INSERTINTO,5465,symboly,14200,symboly,WGS\WN00072434,symboly,ENZO,symboly,MEZZAVILLA,symbolx,5,0,symbolz
INSERTINTO,14625,symboly,14200,symboly,WGS\WN00121862,symboly,MAURO,symboly,PAVAN,symbolx,5,0,symbolz
INSERTINTO,8711,symboly,14200,symboly,WGS\WN00072570,symboly,RALF,symboly,PELLIZZARI,symbolx,5,0,symbolz
INSERTINTO,4843,symboly,14200,symboly,WGS\WN00072984,symboly,SANDRO,symboly,SKARABOT,symbolx,5,0,symbolz
INSERTINTO,14499,symboly,14200,symboly,WGS\WN00118825,symboly,SIMONE,symboly,STUDIO DOGE DI STANCHI,symbolx,5,0,symbolz
--=======================================================================================================================================================
--Token Replacements

--1
--INSERTINTO,
--insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'

--2
--,symboly,
--','

--3
--,symbolx,
--',

--4
--,,symbolz
--);

--=======================================================================================================================================================
--Tranformed for the Token Replacement
declare @ts datetime
set @ts = GETDATE()
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'ROOTID',null,'WGSROOT',' ROOTFIRSTNAME','ROOTLASTNAME',1,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'13670','ROOTID','WGS\WN00087100','CHRISTOPH','LADURNER',2,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'3658','13670','WGS\WN00069099','SERGIO	','SMANIOTTO',3,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'14200','3658','WGS\WN00106285','FABIO	','POZZOBON',4,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'14689','14200','WGS\WN00125448','GHERARDO','BELLONI',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'4023','14200','WGS\WN00071404','MAURIZIO','BERTOLI',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'1208','14200','WGS\WN00071491','ELIA','CHIES',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'10112','14200','WGS\WN00071566','JOSEPH','CONTE',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'13871','14200','WGS\WN00094079','MATTEO','DAL CIN',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'7122','14200','WGS\WN00071832','PAOLO','DE PASCALI',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'3198','14200','WGS\WN00071911','MARCO','FONTANINI',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'6160','14200','WGS\WN00072027','FRANCESCO','GENERO',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'14042','14200','WGS\WN00100884','LUCA','MATURI',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'5465','14200','WGS\WN00072434','ENZO','MEZZAVILLA',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'14625','14200','WGS\WN00121862','MAURO','PAVAN',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'8711','14200','WGS\WN00072570','RALF','PELLIZZARI',5,0);
insert into sf.SalesForceEntity (RowStatus,RowCreationTime,Id,ParentId,SystemAccount,FirstName,LastName,[Level],[Status]) values (1,@ts,'4843','14200','WGS\WN00072984','SANDRO','SKARABOT',5,0);
--Adjust the level
update sf.SalesForceEntity set [Level] = [Level] - 1 --where
--=======================================================================================================================================================
--verify & clean up
select * from sf.SalesForceEntity order by [Level]
delete from sf.SalesForceEntity
--=================================