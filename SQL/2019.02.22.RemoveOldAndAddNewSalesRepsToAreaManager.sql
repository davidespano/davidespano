--Tilo Bohme [10:03 AM]
--Hi @channel. We have received a request from W-Italy to check the sales rep data of Libera Federico:
--Tilo Bohme [10:03 AM]
--1.    Togliere Cottarelli Jura
--2.    Togliere Perbellini Andrea
--3.    Aggiungere Coatti Matteo
--4.    Aggiungere Ferrari Cristian

select * from sf.[User] where Id = 'WGS\WN00073509'
-------------------------------------------------------------------------------------------------------------
--RowStatus	RowCreationTime				RowModificationTime	Id				FirstName	LastName	Status
--1			2018-11-30 16:04:01.9490580	NULL				WGS\WN00073509	FEDERICO	LIBERA			0
-------------------------------------------------------------------------------------------------------------
SELECT SYSDATETIME() 
select * from sf.SalesRep where UserId = 'WGS\WN00073509' order by LastName
---------------------------------------------------------------------------------------------------------------------------
--RowStatus	RowCreationTime				RowModificationTime	Id				FirstName	LastName	Status	UserId
--1			2018-11-30 16:04:01.9490580	NULL				WGS\WN00071708	JURA		COTTARELLI	3		WGS\WN00073509
--1			2018-11-30 16:04:01.9490580	NULL				WGS\WN00073396	ANDREA		PERBELLINI	2		WGS\WN00073509
---------------------------------------------------------------------------------------------------------------------------

--These SalesReps do not presently exist in SpeedyLead
select * from sf.SalesRep where LastName like '%Coatti%'	--WGS\WN00128907 --Coatti Matteo
select * from sf.SalesRep where LastName like '%Ferrari%'   --WGS\WN00128900 --Ferrari Cristian

--delete
--delete from sf.SalesRep where Id = 'WGS\WN00071708' and FirstName = 'JURA' and LastName = 'COTTARELLI'
--delete from sf.SalesRep where Id = 'WGS\WN00073396' and FirstName = 'ANDREA' and LastName = 'PERBELLINI'


--insert
declare @ts datetime2(7) 

set @ts = SYSDATETIME() 

insert into sf.SalesRep 
(Id, FirstName, LastName, Status, UserId, RowStatus, RowCreationTime)
values
('WGS\WN00128907','MATTEO','COATTI',3,'WGS\WN00073509',1,@ts)

insert into sf.SalesRep 
(Id, FirstName, LastName, Status, UserId, RowStatus, RowCreationTime)
values
('WGS\WN00128900','CRISTIAN','FERRARI',3,'WGS\WN00073509',1,@ts)