--These ate the sales Reps to associate
--------------------------------------------------------
--CAMACCI, STEFANO	LONGARINI, NICOLA
--CAMACCI, STEFANO	BECCACECE, MARCO
--CAMACCI, STEFANO	ANGELOZZI, FRANCESCO
--CAMACCI, STEFANO	FORNAROLA, LUCA
--CAMACCI, STEFANO	IALEGGIO, FABIO
--CAMACCI, STEFANO	BOTTICELLI, MARCO
--CAMACCI, STEFANO	AMADORI, FRANCESCO
--CAMACCI, STEFANO	RICHIEDEI, ROBERTO
--CAMACCI, STEFANO	GIOVANNANGELI, WALTER
--CAMACCI, STEFANO	PRIORI, ALBERTO
--CAMACCI, STEFANO	NAPOLETANI, PAOLO
--CAMACCI, STEFANO	SPADONI, DANIEL PABLO
--CAMACCI, STEFANO	GABRIELLI, TIZIANO
--------------------------------------------------------

declare @ts datetime
set @ts = GETDATE()
select @ts

select * from sf.[User] where LastName like '%CAMACCI%' and FirstName like '%STEFANO%'				--WGS\WN00071527

--Update the following SalesReps to the UserId = 
select * from sf.SalesRep where LastName like '%LONGARINI%' and FirstName like '%NICOLA%'			--WGS\WN00072165
select * from sf.SalesRep where LastName like '%BECCACECE%' and FirstName like '%MARCO%'			--WGS\WN00084127
select * from sf.SalesRep where LastName like '%ANGELOZZI%' and FirstName like '%FRANCESCO%'		--WGS\WN00098694
select * from sf.SalesRep where LastName like '%FORNAROLA%' and FirstName like '%LUCA%'				--WGS\WN00103455
select * from sf.SalesRep where LastName like '%IALEGGIO%' and FirstName like '%FABIO%'				--WGS\WN00128886
select * from sf.SalesRep where LastName like '%BOTTICELLI%' and FirstName like '%MARCO%'			--WGS\WN00128898
select * from sf.SalesRep where LastName like '%AMADORI%' and FirstName like '%FRANCESCO%'			--WGS\WN00128932 --<<
select * from sf.SalesRep where LastName like '%RICHIEDEI%' and FirstName like '%ROBERTO%'			--WGS\WN00072816
select * from sf.SalesRep where LastName like '%GIOVANNANGELI%' and FirstName like '%WALTER%'		--WGS\WN00072098
select * from sf.SalesRep where LastName like '%PRIORI%' and FirstName like '%ALBERTO%'				--WGS\WN00072731
select * from sf.SalesRep where LastName like '%NAPOLETANI%' and FirstName like '%PAOLO%'			--WGS\WN00072479
select * from sf.SalesRep where LastName like '%SPADONI%' and FirstName like '%DANIEL PABLO%'		--WGS\WN00073027 --<<
select * from sf.SalesRep where LastName like '%GABRIELLI%' and FirstName like '%TIZIANO%'			--WGS\WN00071981

--update statements
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%LONGARINI%' and FirstName like '%NICOLA%'				 and Id = 'WGS\WN00072165'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%BECCACECE%' and FirstName like '%MARCO%'				 and Id = 'WGS\WN00084127'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%ANGELOZZI%' and FirstName like '%FRANCESCO%'			 and Id = 'WGS\WN00098694'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%FORNAROLA%' and FirstName like '%LUCA%'				 and Id = 'WGS\WN00103455'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%IALEGGIO%' and FirstName like '%FABIO%'				 and Id = 'WGS\WN00128886'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%BOTTICELLI%' and FirstName like '%MARCO%'				 and Id = 'WGS\WN00128898'

update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%RICHIEDEI%' and FirstName like '%ROBERTO%'			 and Id = 'WGS\WN00072816'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%GIOVANNANGELI%' and FirstName like '%WALTER%'			 and Id = 'WGS\WN00072098'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%PRIORI%' and FirstName like '%ALBERTO%'				 and Id = 'WGS\WN00072731'
update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%NAPOLETANI%' and FirstName like '%PAOLO%'				 and Id = 'WGS\WN00072479'

update sf.SalesRep set UserId = 'WGS\WN00071527', RowModificationTime = @ts where LastName like '%GABRIELLI%' and FirstName like '%TIZIANO%'			 and Id = 'WGS\WN00071981'

--These two SalesReps do not yet exist and must be inserted
--select * from sf.SalesRep where LastName like '%AMADORI%' and FirstName like '%FRANCESCO%'			--WGS\WN00128932 --<<
--select * from sf.SalesRep where LastName like '%SPADONI%' and FirstName like '%DANIEL PABLO%'		--WGS\WN00073027 --<<

insert into sf.SalesRep (RowStatus,RowCreationTime,Id,FirstName,LastName,UserId) values (1,@ts,'WGS\WN00128932','AMADORI','FRANCESCO','WGS\WN00071527')
insert into sf.SalesRep (RowStatus,RowCreationTime,Id,FirstName,LastName,UserId) values (1,@ts,'WGS\WN00073027','SPADONI','DANIEL PABLO','WGS\WN00071527')

--------------------------------------------------------
--TIBERTI, FRANCESCO	TANCREDI, PIERO
--TIBERTI, FRANCESCO	D'ALESIO, MATTEO
--TIBERTI, FRANCESCO	GRASSINI, ANDREA
--TIBERTI, FRANCESCO	MARABESE, ANTONIO
--TIBERTI, FRANCESCO	COLELLA, PAUL
--TIBERTI, FRANCESCO	FACCANI, FRANCESCO
--TIBERTI, FRANCESCO	SOLOFRA, FRANCESCO
--TIBERTI, FRANCESCO	BORGACCI, DAVIDE
--TIBERTI, FRANCESCO	PIZZICO, ANDREA
--TIBERTI, FRANCESCO	SANTORO, GUGLIELMO
--TIBERTI, FRANCESCO	SAPUCCI, GIOVANNI
--------------------------------------------------------

--User
select * from sf.[User] where LastName like '%TIBERTI%' and FirstName like '%FRANCESCO%'	--WGS\WN00073088

--SalesReps
select * from sf.SalesRep where LastName like '%TANCREDI%' and FirstName like '%PIERO%'
select * from sf.SalesRep where LastName like '%D%ALESIO%' and FirstName like '%MATTEO%'
select * from sf.SalesRep where LastName like '%GRASSINI%' and FirstName like '%ANDREA%'
select * from sf.SalesRep where LastName like '%MARABESE%' and FirstName like '%ANTONIO%'
select * from sf.SalesRep where LastName like '%COLELLA%' and FirstName like '%PAUL%'
select * from sf.SalesRep where LastName like '%FACCANI%' and FirstName like '%FRANCESCO%'
select * from sf.SalesRep where LastName like '%SOLOFRA%' and FirstName like '%FRANCESCO%'
select * from sf.SalesRep where LastName like '%BORGACCI%' and FirstName like '%DAVIDE%'
select * from sf.SalesRep where LastName like '%PIZZICO%' and FirstName like '%ANDREA%'
select * from sf.SalesRep where LastName like '%SANTORO%' and FirstName like '%GUGLIELMO%'
select * from sf.SalesRep where LastName like '%SAPUCCI%' and FirstName like '%GIOVANNI%'

select Id from sf.SalesRep where LastName like '%TANCREDI%' and FirstName like '%PIERO%'and Id = 'WGS\WN00073104'
select Id from sf.SalesRep where LastName like '%D%ALESIO%' and FirstName like '%MATTEO%'and Id = 'WGS\WN00071788'
select Id from sf.SalesRep where LastName like '%GRASSINI%' and FirstName like '%ANDREA%'and Id = 'WGS\WN00112226'
select Id from sf.SalesRep where LastName like '%MARABESE%' and FirstName like '%ANTONIO%'and Id = 'WGS\WN00112284'
select Id from sf.SalesRep where LastName like '%COLELLA%' and FirstName like '%PAUL%'and Id = 'WGS\WN00125386'
select Id from sf.SalesRep where LastName like '%FACCANI%' and FirstName like '%FRANCESCO%'and Id = 'WGS\WN00071866'
select Id from sf.SalesRep where LastName like '%SOLOFRA%' and FirstName like '%FRANCESCO%'and Id = 'WGS\WN00072985'
select Id from sf.SalesRep where LastName like '%BORGACCI%' and FirstName like '%DAVIDE%'and Id = 'WGS\WN00071358'
select Id from sf.SalesRep where LastName like '%PIZZICO%' and FirstName like '%ANDREA%'and Id = 'WGS\WN00072785'
select Id from sf.SalesRep where LastName like '%SANTORO%' and FirstName like '%GUGLIELMO%'and Id = 'WGS\WN00073020'
select Id from sf.SalesRep where LastName like '%SAPUCCI%' and FirstName like '%GIOVANNI%'and Id = 'WGS\WN00073026'

--update statements
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%TANCREDI%' and FirstName like '%PIERO%'and Id = 'WGS\WN00073104'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%D%ALESIO%' and FirstName like '%MATTEO%'and Id = 'WGS\WN00071788'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%GRASSINI%' and FirstName like '%ANDREA%'and Id = 'WGS\WN00112226'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%MARABESE%' and FirstName like '%ANTONIO%'and Id = 'WGS\WN00112284'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%COLELLA%' and FirstName like '%PAUL%'and Id = 'WGS\WN00125386'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%FACCANI%' and FirstName like '%FRANCESCO%'and Id = 'WGS\WN00071866'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%SOLOFRA%' and FirstName like '%FRANCESCO%'and Id = 'WGS\WN00072985'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%BORGACCI%' and FirstName like '%DAVIDE%'and Id = 'WGS\WN00071358'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%PIZZICO%' and FirstName like '%ANDREA%'and Id = 'WGS\WN00072785'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%SANTORO%' and FirstName like '%GUGLIELMO%'and Id = 'WGS\WN00073020'
update sf.SalesRep set UserId = 'WGS\WN00073088', RowModificationTime = @ts  where LastName like '%SAPUCCI%' and FirstName like '%GIOVANNI%'and Id = 'WGS\WN00073026'


--could you please also add salesrep Lorenzo Matteo 
--to area manager Federico Libera . Thanks a lot
select from WGS\WN00135025
--------------------------------------------------------
