----------------------------------------------------------------------------------
--Part 1
--Map the existing User-Tenant relationship to the Id of the corrisponding user
----------------------------------------------------------------------------------

--Update Pozzobon WGS\WN00106285 on all tenants
update cat.UserTenantRelation set 
	AppUserId = '14200' 
where 
	UserId = 'WGS\WN00106285' 

-- WGS\WN00079555 is mapped to WGS\WN00106285 thus
update cat.UserTenantRelation set 
	AppUserId = '14200' 
where 
	UserId = 'WGS\WN00079555' 

--WGS\WN00057195 is mapped to WGS\WN00106285 but only on PHXDEV, PHXTEST, PHXPROD thus
update cat.UserTenantRelation set 
	AppUserId = '14200' 
where 
	UserId = 'WGS\WN00057195' 
	and 
    (TenantId = 'PHXDEV' OR TenantId = 'PHXTEST' OR TenantId = 'PHXPROD' OR TenantId = 'WITTEST')

--update WGS\WN00057195 on all tenants - not found

-- update (1,@ts,'6851','3139','WGS\WN00071560','GIORGIO','CANNATELLA',4,0), on all tenants
update cat.UserTenantRelation set 
	AppUserId = '6851' 
where 
	UserId = 'WGS\WN00071560' 	

--------------------------------------------------------------------------------------
--Part 1

--(1,@ts,'13670','ROOTID','WGS\WN00087100','CHRISTOPH','LADURNER',2,0),     >> Region Manager   L1 .. WP\psl0001
--(1,@ts,'3658','13670','WGS\WN00069099','SERGIO','SMANIOTTO',3,0),         >> Branch Manger    L2 >> WP\psl0002
--(1,@ts,'14200','3658','WGS\WN00106285','FABIO','POZZOBON',4,0),			>> AreaManger		L3 

--Map WP\psl0002 to  LEVEL20 (Regional Manager) over POZZOBON WGS\WN00106285 = 14200

--------------------------------------------------------------------------------------

update cat.UserTenantRelation set 
	AppUserId = '3658' 
where 
	UserId = 'WP\psl0002' 	


update cat.UserTenantRelation set 
	AppUserId = '13670' 
where 
	UserId = 'WP\psl0001' 	