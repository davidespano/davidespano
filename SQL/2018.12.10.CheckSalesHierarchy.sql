--user Calvini Ivan noticed that there are two sales reps missing that he expected to see:
--Josue Martinez
--Mattia Vimercati
--can u please check why they are not in the system? thanks a lot!

use [SL_WIT_PROD]
go

--Find the user Calvini Ivan 
select * from sf.[User] where LastName like '%Calvini%'

--------------------------------------------------------------------------------------------------------------
--RowStatus	RowCreationTime				RowModificationTime	Id				FirstName	LastName	Status
--1			2018-11-30 16:04:01.9490580	NULL				WGS\WN00071525	IVAN		CALVINI	    0
--------------------------------------------------------------------------------------------------------------

--check the SalesReps that are managed by this user
select u.Id, u.FirstName, u.LastName, r.* 
from sf.SalesRep r
join sf.[User] u
on r.UserId = u.Id
where u.Id IN (select Id from sf.[User] where LastName like '%Calvini%')

--user Calvini Ivan noticed that there are two sales reps missing that he expected to see:
--Josue Martinez >> JOSUE MARTINEZ RABOLLI
--Mattia Vimercati

--These are all the Mattia in the original data
--MATTIA CARDINETTI
--MATTIA BEROGGI
--MATTIA FERRANTE
--MATTIA BOLOGNESI
--MATTIA AUDITORE
--MATTIA ANGELOSANTI






  