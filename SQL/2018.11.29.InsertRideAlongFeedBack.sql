use SLDemoTenant go

declare @salesRepId varchar(50) = 'TestSalesRepId';

--insert ride along on todays date
insert into cal.RideAlong (
RowStatus, 
RowCreationTime, 
RowModificationTime,
[User],
[Day],
SalesRepId,
[Time],
Mode,
[Status],
ResponseNote)
values(
0,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'TestUser',
CURRENT_TIMESTAMP,
@salesRepId,
0,
0,
0,
'ResponseNote')

--declare @salesRepId varchar(50) = 'TestSalesRepId';
declare @rideAlongId int = (select top 1 Id from cal.RideAlong ra where ra.SalesRepId = @salesRepId)
--select @rideAlongId

----------------------------------------------------------------------------------

--insert ride along feedback 
insert into raf.RideAlongFeedback
(
RowStatus, 
RowCreationTime, 
RowModificationTime,
RideAlongId,
[Status]
)
values
(
0,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
@rideAlongId,
0
)

----------------------------------------------------------------------------------

--insert a ridealong feedback value
--declare @salesRepId varchar(50) = 'TestSalesRepId';
--declare @rideAlongId int = (select top 1 Id from cal.RideAlong ra where ra.SalesRepId = @salesRepId)
--set @rideAlongId = (select top 1 Id from cal.RideAlong ra where ra.SalesRepId = @salesRepId)
--select @rideAlongId

declare @rideAlongFeedbackId int = (
select top 1 Id 
from raf.RideAlongFeedback f 
where f.RideAlongId = @rideAlongId)

--select  @rideAlongFeedbackId

insert into raf.RideAlongFeedbackRateValue
(
RowStatus, 
RowCreationTime, 
RowModificationTime,
Identifier,
[Value],
RideAlongFeedbackId
)
values
(
0,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'Identifier',
0,
@rideAlongFeedbackId 
)
