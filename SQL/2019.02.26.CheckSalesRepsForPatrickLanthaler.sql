--@channel, sorry for the boring bad "news", but the customer asked us to verify 
--the sales reps visible for one of the new pilots: Patrick Lanthaler. He does not 
--see all the sales reps he should see. This should be the correct sales reps to 
--visualize:

select * from sf.[User] where FirstName like '%Patrick%' and LastName like '%Lanthaler%' 
-------------------------------------------------------------------------------------------------------------
--RowStatus	RowCreationTime				RowModificationTime	Id				FirstName	LastName	Status
--1			2018-11-30 16:04:01.9490580	NULL				WGS\WN00072170	PATRICK		LANTHALER	0
-------------------------------------------------------------------------------------------------------------

select * from sf.SalesRep where UserId = 'WGS\WN00072170' order by LastName
----------------------------------------------------------------------------
--Must add these two
--2 LANTHALER, PATRICK	5232	COTTARELLI, JURA		>> ADD >> WGS\WN00071708	JURA COTTARELLI
--12 LANTHALER, PATRICK	14871	PLATTER, PATRICK		>> ADD >> WGS\WN00131828
----------------------------------------------------------------------------
--These is the list of SalesReps that should be present
--1 LANTHALER, PATRICK	9643	CLEMENTI, CHRISTIAN
--2 LANTHALER, PATRICK	5232	COTTARELLI, JURA		>> ADD
--3 LANTHALER, PATRICK	8492	DIPOLI, EGON
--4 LANTHALER, PATRICK	8242	ERB, HEINO
--5 LANTHALER, PATRICK	10342	FROETSCHER, CHRISTIAN
--6 LANTHALER, PATRICK	14703	GUMMERER, SIMON
--7 LANTHALER, PATRICK	5587	HOELLER, KARL
--10 LANTHALER, PATRICK	14566	MAIR, ROBERT
--11 LANTHALER, PATRICK	14602	MAIRHOFER, FRANCESCO
--12 LANTHALER, PATRICK	14871	PLATTER, PATRICK		>> ADD
--13 LANTHALER, PATRICK	6872	RABANSER, HELMUT
--14 LANTHALER, PATRICK	14128	SCHRAFFL, BENJAMIN
--15 LANTHALER, PATRICK	6341	VOLGGER, ROLAND
--16 LANTHALER, PATRICK	12186	ZOESCHG, MARTIN
---------------------------------------------------------------------------
--8 LANTHALER, PATRICK	10563	LANTHALER, PATRICK

--insert
declare @ts datetime2(7) 

set @ts = SYSDATETIME() 

insert into sf.SalesRep 
(Id, FirstName, LastName, Status, UserId, RowStatus, RowCreationTime)
values
('WGS\WN00071708','JURA','COTTARELLI',3,'WGS\WN00072170',1,@ts)

insert into sf.SalesRep 
(Id, FirstName, LastName, Status, UserId, RowStatus, RowCreationTime)
values
('WGS\WN00131828','PLATTER','PATRICK',3,'WGS\WN00072170',1,@ts)
