/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [UserId]
      ,[TenantId]
      ,[ApplicationType]
      ,[AppUserId]
      ,[ApplicationId]
  FROM [cat].[UserTenantRelation]

--WN00802522 (Dominik Fischer)
SELECT * FROM [cat].[UserTenantRelation] where UserId like 'WGS\WN00802522'
--WN00120411 (Kai Bruchmüller)
SELECT * FROM [cat].[UserTenantRelation] where UserId like 'WGS\WN00120411'

--(1,@ts,'14197','3495','WGS\WN00106284','ANDREA','MARCONCINI',4,0),
--(1,@ts,'13740','3658','WGS\WN00087099','CRISTIAN','CASAGRANDE',4,0),
------------------------------------------------------------------------------
--UserId			TenantId	ApplicationType	AppUserId		ApplicationId
--WGS\WN00802522	WITTEST		speedylead		WGS\WN00106284	NULL
--WGS\WN00120411	WITTEST		speedylead		WGS\WN00087099	NULL
------------------------------------------------------------------------------

 update cat.UserTenantRelation set 
	AppUserId = '14197' 
where 
	UserId = 'WGS\WN00802522' 
	and 
    (TenantId = 'WITTEST')

 update cat.UserTenantRelation set 
	AppUserId = '13740' 
where 
	UserId = 'WGS\WN00120411' 
	and 
    (TenantId = 'WITTEST')