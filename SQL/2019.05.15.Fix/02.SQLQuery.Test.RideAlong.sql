/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [RowStatus]
      ,[RowCreationTime]
      ,[RowModificationTime]
      ,[Id]
      ,[User]
      ,[Day]
      ,[SalesRepId]
      ,[Order]
      ,[Mode]
      ,[Status]
      ,[ResponseNote]
      ,[FeedbackStatus]
  FROM [cal].[RideAlong]

 select * from [cal].[RideAlong]
where Id IN 
(
	2386,
	2407,
	2408,
	2410,
	2411,
	2412,
	2413,
	2414,
	2421
)
order by SalesRepId
--order by Id

--RowStatus	RowCreationTime	RowModificationTime	Id	User	Day	SalesRepId	Order		Mode			Status	ResponseNote	FeedbackStatus
--0	2019-04-18 08:37:46.0197880	2019-05-14 16:04:29.3100000	2386	7005	2019-04-18	14592			0	1	3	NULL	1
--0	2019-04-19 11:44:12.7131021	2019-05-14 16:04:29.3100000	2407	7005	2019-04-19	13946			0	1	3	NULL	1
--0	2019-04-19 11:47:01.6578896	2019-05-14 16:04:29.3100000	2408	7005	2019-04-22	14685			0	1	3	NULL	1
--0	2019-04-23 20:52:53.4371645	2019-05-14 16:04:29.3100000	2410	7005	2019-04-23	WGS\WN00122752	0	1	3	NULL	1
--0	2019-04-23 20:53:26.3534412	2019-05-14 16:04:29.3100000	2411	7005	2019-04-24	14592			1	1	3	NULL	1
--0	2019-04-23 20:53:26.3534412	2019-05-14 16:04:29.3100000	2412	7005	2019-04-25	WGS\WN00122752	1	1	3	NULL	1
--0	2019-04-23 20:53:26.3534412	2019-05-14 16:04:29.3100000	2413	7005	2019-04-26	14685			1	1	3	NULL	2
--0	2019-04-23 20:53:26.3534412	2019-05-14 16:04:29.3100000	2414	7005	2019-04-29	14687			1	1	3	NULL	1
--0	2019-05-02 08:58:19.3105070	2019-05-14 16:04:29.3100000	2421	7005	2019-05-02	14685			0	1	3	NULL	2

--Here we have problems with some of the ridealongs that still have WGS system account as their
--SalesRepId instead of the corrisponding Id