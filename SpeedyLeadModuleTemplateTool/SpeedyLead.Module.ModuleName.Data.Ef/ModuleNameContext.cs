﻿using Microsoft.EntityFrameworkCore;
using SpeedyLead.Library.Data.Ef.Common;
using SpeedyLead.Module.$modulename$.Common;
using SpeedyLead.Module.$modulename$.Common.Entities;
using SpeedyLead.Project.Common;

namespace SpeedyLead.Module.ModuleName.Data.Ef
{
    public class ModuleNameContext : SpeedyLeadDbContext<ModuleNameContext>
    {
    }
}
