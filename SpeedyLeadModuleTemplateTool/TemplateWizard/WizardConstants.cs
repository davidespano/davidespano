﻿
namespace TemplateWizard
{
    internal static class WizardConstants
    {
        internal const string SafeProjectName = @"$safeprojectname$";
        internal const string SafeRootProjectName = @"$saferootprojectname$";
        internal const string ModuleName = @"$modulename$";
        internal const string ModuleNameCamelCase = @"$modulenamecamelcase$";
    }
}
