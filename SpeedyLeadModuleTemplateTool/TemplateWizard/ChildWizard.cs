﻿using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using System.Collections.Generic;

namespace TemplateWizard
{
    public class ChildWizard : IWizard
    {
        // Retrieve global replacement parameters
        public void RunStarted(object automationObject, 
            Dictionary<string, string> replacementsDictionary, 
            WizardRunKind runKind, object[] customParams)
        {
            // Add custom parameters.
            replacementsDictionary.Add(WizardConstants.SafeRootProjectName,
                RootWizard.GlobalDictionary[WizardConstants.SafeRootProjectName]);

            replacementsDictionary.Add(WizardConstants.ModuleName,
                RootWizard.GlobalDictionary[WizardConstants.ModuleName]);

            replacementsDictionary.Add(WizardConstants.ModuleNameCamelCase,
                RootWizard.GlobalDictionary[WizardConstants.ModuleNameCamelCase]);
        }

        public void RunFinished()
        {
        }

        public void BeforeOpeningFile(ProjectItem projectItem)
        {
        }

        public void ProjectFinishedGenerating(Project project)
        {
        }

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
        }
    }
}
