﻿using System;
using System.Collections.Generic;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;

namespace TemplateWizard
{
    // Root wizard is used by root project vstemplate

    public class RootWizard : IWizard
    {
        public static Dictionary<string, string> GlobalDictionary =
            new Dictionary<string,string>();

        public void RunStarted(
            object automationObject, 
            Dictionary<string, string> replacementsDictionary, 
            WizardRunKind runKind, 
            object[] customParams)
        {
            GlobalDictionary[WizardConstants.SafeRootProjectName] = replacementsDictionary[WizardConstants.SafeProjectName];
            GlobalDictionary[WizardConstants.ModuleName] = replacementsDictionary[WizardConstants.SafeProjectName];

            var moduleName = GlobalDictionary[WizardConstants.ModuleName];
            var moduleNameArray = moduleName.ToCharArray();
            GlobalDictionary[WizardConstants.ModuleNameCamelCase] = Char.ToLowerInvariant(moduleNameArray[0]) + moduleName.Substring(1);
        }

        public void RunFinished()
        {
        }

        public void BeforeOpeningFile(ProjectItem projectItem)
        {
        }

        public void ProjectFinishedGenerating(Project project)
        {
        }

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
        }
    }
}
