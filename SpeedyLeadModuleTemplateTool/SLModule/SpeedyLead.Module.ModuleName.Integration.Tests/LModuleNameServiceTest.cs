using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using SpeedyLead.Library.Common;
using SpeedyLead.Library.Common.Models;
using SpeedyLead.Library.Integration.Test.Common;
using SpeedyLead.Library.Test.Common;
using SpeedyLead.Library.Test.Common.Stubs;
using SpeedyLead.Library.Test.Data;
using SpeedyLead.Module.$modulename$.Common;
using SpeedyLead.Module.$modulename$.Data.Ef;
using Xunit;
using Xunit.Abstractions;

namespace SpeedyLead.Module.$modulename$.Integration.Tests
{
    [Trait(TestConstants.Category, TestConstants.Integration)]
    public class L$modulename$ServiceTest : IntegrationTestBase
    {
        public L$modulename$ServiceTest(ITestOutputHelper testOutputHelper) 
            : base(testOutputHelper) { }

        [Fact]
        public void Ef$modulename$Service_Must_Implement_I$modulename$Service()
        {
            // arrange
            // act
            var $modulenamecamelcase$Service = Create$modulename$Service();

            // assert
            $modulenamecamelcase$Service
                .Should()
                .BeOfType<L$modulename$Service>();

            ($modulenamecamelcase$Service as I$modulename$Service)
                .Should()
                .NotBeNull();
        }

        private I$modulename$Service Create$modulename$Service()
        {
            var serviceCollection = new ServiceCollection();

            var serviceProvider = serviceCollection
                .AddTransient<IServiceConfiguration, ServiceConfigurationStub>()
                .AddTransient<IContext>(provider => new ContextStub(SpeedyLeadTestDataConstants.AreaManagerUserId1))
                .AddTransient(provider => IntegrationTestContextOptionsService<CalendarContext>.CreateOptions(_integrationTestDatabaseSetup))
                .AddTransient<I$modulename$Dac,Ef$modulename$Dac>()
                .AddTransient<I$modulename$Service,L$modulename$Service>()
                .BuildServiceProvider();

            var l$modulename$Service = serviceProvider
                .GetService<I$modulename$Service > ();

            return l$modulename$Service;
        }
    }
}
