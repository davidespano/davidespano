using System;
using System.Linq;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using SpeedyLead.Library.Common;
using SpeedyLead.Library.Common.Models;
using SpeedyLead.Library.Integration.Test.Common;
using SpeedyLead.Library.Test.Common;
using SpeedyLead.Library.Test.Common.Stubs;
using SpeedyLead.Library.Test.Data;
using SpeedyLead.Module.$modulename$.Common;
using SpeedyLead.Module.$modulename$.Common.Entities;
using SpeedyLead.Module.$modulename$.Data.Ef;
using Xunit;
using Xunit.Abstractions;

namespace SpeedyLead.Module.$modulename$.Integration.Tests
{
    [Trait(TestConstants.Category, TestConstants.Integration)]
    public class Ef$modulename$DacTest : IntegrationTestBase
    {
    
        public Ef$modulename$Test(
                ITestOutputHelper testOutputHelper) 
                : base(testOutputHelper) { }

        [Fact]
        public void Ef$modulename$Dac_Must_Implement_I$modulename$Dac()
        {
            // arrange
            // act
            var ef$modulename$Dac = CreateEf$modulename$Dac();

            // assert
            ef$modulename$Dac
            .Should()
                .BeOfType<Ef$modulename$Dac>();

            (ef$modulename$Dac as I$modulename$Dac)
                .Should()
                .NotBeNull();
         }

        private I$modulename$ CreateEf$modulename$Dac()
        {
            var serviceCollection = new ServiceCollection();

            var serviceProvider = serviceCollection
                .AddTransient<IServiceConfiguration, ServiceConfigurationStub>()
                .AddTransient<IContext>(provider => new ContextStub(SpeedyLeadTestDataConstants.AreaManagerUserId1))
                .AddTransient(provider => IntegrationTestContextOptionsService<$modulename$Context>.CreateOptions(_integrationTestDatabaseSetup))
                .AddTransient<I$modulename$Dac, Ef$modulename$Dac>()
                .BuildServiceProvider();

            var efCalendarDac = serviceProvider
                .GetService< I$modulename$Dac> ();

            return (Ef$modulename$Dac)ef$modulename$Dac;
        }
    }
}
