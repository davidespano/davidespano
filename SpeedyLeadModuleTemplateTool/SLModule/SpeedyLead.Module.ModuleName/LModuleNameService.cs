﻿using System;
using System.Linq;
using SpeedyLead.Library.Common;
using SpeedyLead.Library.Common.Models;
using SpeedyLead.Module.$modulename$.Common;
using SpeedyLead.Module.$modulename$.Common.Entities;

namespace SpeedyLead.Module.$modulename$
{
    public class L$modulename$Service : 
    BaseLocalService,
    I$modulename$Service
    {
        private readonly I$modulename$Dac _$modulenamecamelcase$Dac;

        public L$modulename$Service(
                IServiceConfiguration serviceConfiguration,
                IContext context,
                I$modulename$Dac $modulenamecamelcase$Dac)
                : base(serviceConfiguration, context)
        {
            _$modulenamecamelcase$Dac = $modulenamecamelcase$Dac;
        }
    }
}
