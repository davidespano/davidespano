﻿using Microsoft.EntityFrameworkCore;
using SpeedyLead.Library.Data.Ef.Common;
using SpeedyLead.Module.$modulename$.Common;
using SpeedyLead.Module.$modulename$.Common.Entities;
using SpeedyLead.Project.Common;

namespace SpeedyLead.Module.$modulename$.Data.Ef
{
    public class $modulename$Context : SpeedyLeadDbContext<$modulename$Context>
    {
        private static $modulename$Context _internalContext;

        public $modulename$Context()
        {
            // required by EF Core on generating migration scripts from command line or build tools.
        }

        private CalendarContext(DbContextOptions<$modulename$Context> options)
        : base(options) { }

        public static $modulename$Context NewContext(string connection)
        {
            if (_internalContext != null)
                return _internalContext;

            var contextOptions = ContextFactory.BuildContextOptions<$modulename$Context> (connection);
            return new $modulename$Context(contextOptions);
        }

        public static $modulename$Context NewContext(DbContextOptions<$modulename$Context> options)
        {
            if (_internalContext != null)
                return _internalContext;

            return new $modulename$Context(options);
        }

        public virtual DbSet<SomeEntity> SomeEntity { get; set; }

        public override string SchemaName => SpeedyLeadProjectConstants.SchemaName$modulename$;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            if (!optionsBuilder.IsConfigured)
            {
                Configure<I$modulename$Dac > (optionsBuilder);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingSomeEntity(modelBuilder);
        }

        private void OnModelCreatingSomeEntity(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<SomeEntity>()
                .ToTable(
                    name: nameof(Common.Entities.SomeEntity),
                    schema: this.SchemaName);
        }
    }
}
