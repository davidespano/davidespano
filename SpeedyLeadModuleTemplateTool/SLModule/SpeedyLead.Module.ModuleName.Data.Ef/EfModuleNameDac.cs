﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using SpeedyLead.Library.Common;
using SpeedyLead.Library.Common.Models;
using SpeedyLead.Library.Data.Ef.Common;
using SpeedyLead.Module.$modulename$.Common;
using SpeedyLead.Module.$modulename$.Common.Entities;
using SpeedyLead.Module.$modulename$.Common.ReferencedEntities;
using SpeedyLead.Utils;

namespace SpeedyLead.Module.$modulename$.Data.Ef
{

    private readonly DbContextOptions<$modulename$Context> _$modulenamecamelcase$DbContextOptions;

    public class Ef$modulename$Dac : 
        BaseDac, 
        I$modulename$Dac
    {
        DbContextOptions<$modulename$Context> _$modulenamecamelcase$DbContextOptions;
    }

    public Ef$modulename$Dac(
        IServiceConfiguration serviceConfiguration,
        IContext context,
        IContextOptions<$modulename$Context> $modulenamecamelcase$ContextOptions) 
        : base(serviceConfiguration,context)
    {
        if (serviceConfiguration == null ||
        context == null ||
        $modulenamecamelcase$ContextOptions == null)
        {
            throw new ArgumentNullException();
        }

        _$modulenamecamelcase$DbContextOptions = $modulenamecamelcase$ContextOptions.Options;
    }
}
