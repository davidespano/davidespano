using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SpeedyLead.Library.Common;
using SpeedyLead.Library.Common.Models;
using SpeedyLead.Library.Test.Common;
using SpeedyLead.Library.Test.Common.Mocks;
using SpeedyLead.Library.Test.Common.Stubs;
using SpeedyLead.Module.$modulename$.Common;
using SpeedyLead.Module.$modulename$.Common.Entities;
using SpeedyLead.Module.$modulename$.Data.Ef;
using Xunit;
using Xunit.Abstractions;

namespace SpeedyLead.Module.$modulename$.Tests
{
    public class L$modulename$ServiceTest
    {
        private class $modulename$DacMock : 
            BaseDacMock, 
            I$modulename$Dac
        {
        }

        public LCalendarServiceTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _testOutputHelper.WriteLine(nameof(L$modulename$ServiceTest));
        }

        [Fact]
        public void L$modulename$Service_Implements_I$modulename$Service()
        {
            // arrange
            // act
            var $modulenamecamelcase$Service = Create$modulename$Service();

            // assert
            $modulenamecamelcase$Service
                .Should()
                .BeOfType<L$modulename$Service>();

            ($modulenamecamelcase$Service as I$modulename$Service)
                .Should()
                .NotBeNull();
        }

        private I$modulename$Service Create$modulename$Service()
        {
            var serviceCollection = new ServiceCollection();

            var serviceProvider = serviceCollection
                .AddTransient<IServiceConfiguration, ServiceConfigurationStub>()
                .AddTransient<IContext>(provider => new ContextStub(Constants.AreaManagerUserId1))
                .AddTransient<I$modulename$Dac, $modulename$DacMock>()
                .AddTransient<I$modulename$Service,L$modulename$Service>()
                .BuildServiceProvider();

            var l$modulename$Service = serviceProvider
                .GetService<I$modulename$Service>();

            return l$modulename$Service;
        }
    }
}
